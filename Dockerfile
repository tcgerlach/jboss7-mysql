FROM tutum/jboss:as7
MAINTAINER Thomas Gerlach <info@talixa.com>

ADD com.mysql.jdbc_5.1.27.jar /jboss-as-7.1.1.Final/modules/com/mysql/main/com.mysql.jdbc_5.1.27.jar
ADD module.xml /jboss-as-7.1.1.Final/modules/com/mysql/main/module.xml
ADD standalone.xml /jboss-as-7.1.1.Final/standalone/configuration/standalone.xml

