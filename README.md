Usage
-------
docker run -d -v ~/app.war:/jboss-as-7.1.1.Final/standalone/deployments/ --link mysql-instance:mysql tcgerlach/jboss7-mysql

Default MySQL DataSource Setup
-------
JNDI: mysql
MySQL Username: mysqluser
MySQL Password: password
These can be changed in /jboss-as-7.1.1.Final/standalone/configuration/standalone.xml

Git Repository
--------------
https://bitbucket.org/tcgerlach/jboss7-mysql.git

IMPORTANT
--------
This is a beta image and is not thoroughly tested yet.

